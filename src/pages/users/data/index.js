/* eslint-disable react/prop-types */
/* eslint-disable react/function-component-definition */

import {NavLink} from "react-router-dom";
import { Button } from "@mui/material";
export default function data(users,deleteCallback) {  
    
  return {
    
    columns: [
      { Header: "name", accessor: "name", align: "left" },
      { Header: "email", accessor: "email", align: "left" },
      { Header: "phone Number", accessor: "phoneNumber", align: "center" },      
      { Header: "username", accessor: "username", align: "center" },
      { Header: "website", accessor: "website", align: "center" },
      { Header: "actions", accessor: "actions", align: "center" },
      
    ],

    rows: [
      ...users.map((user,index)=>RowData(user,index),)
    ],
  };
  function RowData(rowData,index) {
    
    return {
      name: rowData.name,
      email: rowData.email.toLowerCase(),
      phoneNumber: rowData.phone,
      username: rowData.username,
      website: rowData.website,
      actions: <>
      <NavLink                    
      to={`/user/${rowData.id}`}
      key={'/userDetails'}                                    
    >View details</NavLink>
      
        <Button onClick={()=> deleteCallback(rowData._id,index)}>Delete</Button>
      </>
      
    }
  }
}

// @mui material components
import {Card, Backdrop, CircularProgress, Box, Typography} from "@mui/material";
import { useEffect, useState,  } from "react"

import DashboardLayout from "layout/DashboardLayout";


import { useApiContext } from "services";
import { useParams } from 'react-router';


function UserDetails() {  
  const apiService = useApiContext()
  const [state, setState] = useState({    
    user: null
  })
  const { id } = useParams();

  const [pageState, setPageState] = useState({
    isLoading: true,
    error: ""
  })
  
  useEffect(() => {
    async function fetchData() {      
      try {      
        const user = (await apiService.get(`users/${id}`))
        console.log(user)
        setState({ user }) 
        setPageState({...pageState, isLoading:false})
      }
      catch (e) {
        setPageState({ ...pageState, error:e })
      }
    }
    fetchData()
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  
  
    return (
    <DashboardLayout>
      <Box pt={4} pb={3}>
        
        {pageState.isLoading ?
          <Backdrop
            sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
            open={pageState.isLoading}>
            <CircularProgress color="inherit" />
          </Backdrop> :
          <Card>
            <Box
              variant="gradient"              
              mx={2}
              mt={-3}
              p={2}
              mb={1}
              textAlign="center"
            >
              <Typography variant="h4" fontWeight="light" mt={1}>
                User details
              </Typography>
              </Box>
            {pageState.error && <Typography color="red">{pageState.error}</Typography>}
            <Box pt={4} pb={3} px={3}>
                <Card display={"flex"}>
                  <Typography>
                    User name
                  </Typography>
                  <Typography>
                    {state.user.name}
                  </Typography>
                </Card>              
                <Card display={"flex"}>
                  <Typography>
                    Phone number
                  </Typography>
                  <Typography>
                    {state.user.phone}
                  </Typography>
                </Card>              
            </Box>
            </Card>
          }
      </Box>
    </DashboardLayout>
  );
}

export default UserDetails;

import React from "react";
import { render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { useApiContext } from "services";
import Users from "./Users";

jest.mock("services");

describe("Users component", () => {
    const mockGet = jest.fn();
    const mockApiService = {
        get: mockGet,
    };

    beforeEach(() => {
        useApiContext.mockReturnValue(mockApiService);
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    test("renders loading spinner when API call is in progress", async () => {
        mockGet.mockResolvedValue([]);

        render(<Users />);

        expect(screen.getByRole("progressbar")).toBeInTheDocument();

        await waitFor(() => {
            expect(screen.queryByRole("progressbar")).not.toBeInTheDocument();
        });
    });

    test("renders error message when API call fails", async () => {
        const errorMessage = "Error message";
        mockGet.mockRejectedValue({ message: errorMessage });

        render(<Users />);

        await waitFor(() => {
            expect(screen.getByText(errorMessage)).toBeInTheDocument();
        });
    });

    test("renders table when API call succeeds", async () => {
        const users = [
            { id: 1, name: "Alice" },
            { id: 2, name: "Bob" },
        ];
        mockGet.mockResolvedValue(users);

        render(<Users />);

        await waitFor(() => {
            expect(screen.getByRole("table")).toBeInTheDocument();
        });

        expect(screen.getByText(users[0].name)).toBeInTheDocument();
        expect(screen.getByText(users[1].name)).toBeInTheDocument();
    });
})
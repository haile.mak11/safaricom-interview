// @mui material components
import Grid from "@mui/material/Grid";

// Material Dashboard 2 React components
import {Box} from "@mui/material";
import {Typography} from "@mui/material";
import {
  Backdrop, CircularProgress, 
} from "@mui/material"
// Material Dashboard 2 React example components
import DashboardLayout from "../../layout/DashboardLayout"; 

import DataTable from "../../components/DataTable"; 

// Data
import data from "./data";
import { useState, useEffect } from "react";
import {  useApiContext } from "services";


function Users() {
  const apiService = useApiContext()
  const [users, setUsers] = useState([]);  
  const [pageState, setPageState] = useState({
    isLoading: true,
    isSuccess: false,
    error:null,
  })
  useEffect(() => {
    async function fetchData() {      
      try {        
        const users = (await apiService.get('users'))
        console.log(users)        
        setUsers(users) 
        setPageState({...pageState,isSuccess:false,isLoading:false})
      } catch (error) {
        setPageState({ ...pageState, error:error.response?.data?.message??error.message, isLoading: false });
        console.log(error);
      }
    }
    fetchData()
    
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const deleteCallback= async (id,index)=> {      
    try {        
      users.splice(index,1)
      setUsers([...users,])
      setPageState({...pageState,isSuccess:false,isLoading:false})
    } catch (error) {
      console.log(error)
      setPageState({ ...pageState, error:error.response?.data?.message??error.message, isLoading: false });
    }
  }
  const { columns, rows } = data(users,deleteCallback);
  
  
  return (
    <DashboardLayout>
      <Box pt={4} pb={3}>
        <Grid container spacing={6}>
          <Grid item xs={12}>                         
            <Backdrop
              sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
              open={pageState.isLoading}>
              <CircularProgress color="inherit" />
            </Backdrop>
            {pageState.error ?
            <Box px={3}>
              <Typography fontWeight="light" color="error"  mt={1}>
                {pageState.error??""}
              </Typography>
            </Box> :
            <Box >
            
              <Box pt={2}>
                <DataTable
                  table={{ columns, rows }}                  
                  isSorted={true}
                  entriesPerPage={true}
                  showTotalEntries={true}
                  pagination={ {variant: "gradient", color: "info" }}
                  canSearch
                />
              </Box>
            </Box>}
          </Grid>          
        </Grid>
      </Box>
    </DashboardLayout>
  );
}

export default Users;

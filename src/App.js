import { Routes, Route, Navigate } from "react-router-dom";
import routes from "./routes";

function App() {
  const getRoutes = (allRoutes) =>
    allRoutes.map((route) => {
      if (route.collapse) {
        return getRoutes(route.collapse);
      }

      if (route.route) {
        return <Route exact path={route.route} element={route.component} key={route.key} />;
      }

      return null;
    }); 
  
  return (   
      <Routes>
        {getRoutes(routes)}
        <Route path="/users" element={<Navigate to="/users" />} />
        <Route path="*" element={<Navigate to="/users" />} />
      </Routes>
    
  );
}

export default App;

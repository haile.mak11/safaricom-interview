// Material Dashboard 2 React components
import { Box } from "@mui/material";

// Material Dashboard 2 React context


function DashboardLayout({ children }) {  

  return (
    <Box>
      {children}
    </Box>
  );
}


export default DashboardLayout;

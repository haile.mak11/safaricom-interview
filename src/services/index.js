import ApiService from "./api";
import {createContext,useContext} from "react"
const ApiContext = createContext();
const { Provider } = ApiContext;
const ApiProvider = ({ children }) => {
  const apiService = new ApiService();
  return <ApiContext.Provider value={ apiService }>{children}</ApiContext.Provider>;
};
function useApiContext() {
    const context = useContext(ApiContext);
  
    if (!context) {
      throw new Error(
        "useApiContext should be used inside the ApiProvider."
      );
    }
  
    return context;
  }
export { ApiContext, ApiProvider, useApiContext }
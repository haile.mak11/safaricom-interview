import axios from "axios";

export default class ApiService {
    constructor() {
        this.axiosInstance = axios.create({
            baseURL: "https://jsonplaceholder.typicode.com/",
        });        
        
    }
    async get(resource) {
        const response = await this.axiosInstance.get(resource)
        return response.data
    }
    async post(resource,body) {
        const response = await this.axiosInstance.post(resource,body)
        return response.data
    }
    async delete(resource,body) {
        const response = await this.axiosInstance.delete(resource,body)
        return response.data
    }
    put() {}
    
}


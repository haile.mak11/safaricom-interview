import UserDetails from "pages/users/components/User";
import Users from "./pages/users";

const routes = [      
    
  {    
    key: "users",
    route: "/users",
    component: <Users />,
  },
  {    
    key: "userDetail",
    route: "/user/:id",
    component: <UserDetails />,
  }
];

export default routes;
